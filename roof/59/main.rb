ARGV[0] = 'input.txt'

mappings = {
    0 => '0',
    1 => '1',
    2 => 'abc',
    3 => 'def',
    4 => 'ghi',
    5 => 'jkl',
    6 => 'mno',
    7 => 'pqrs',
    8 => 'tuv',
    9 => 'wxyz'
}

File.open(ARGV[0]).each_line do |line|
  nums = line.strip.chars.map(&:to_i)
  output = mappings[nums.shift].chars.map { |char| [char] }
  nums.each do |num|
    new_output = []
    output.each do |out|
      mappings[num].chars.each do |mapped|
        new_output << out + [mapped]
      end
    end
    output = new_output
  end
  puts output.map(&:join).sort.join(',')
end