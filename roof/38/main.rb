ARGV[0] = 'input.txt'

def get_perms(alphabet, perm_size)
  return [] if perm_size.zero?
  perms = alphabet.map { |char| [char] }
  (perm_size-1).times do
    new_perms = []
    perms.each do |perm|
      alphabet.each do |char|
        new_perms << perm + [char]
      end
    end
    perms = new_perms
  end
  perms
end

File.open(ARGV[0]).each_line do |line|
  perm_size, alphabet = line.strip.split(',')
  perm_size = perm_size.to_i
  alphabet = alphabet.chars
  puts get_perms(alphabet, perm_size).uniq.map(&:join).sort.join(',')
end