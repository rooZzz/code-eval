ARGV[0] = 'input.txt'

File.open(ARGV[0]).each_line do |line|
  words = line.split(' ').map do |word|
    chars = word.strip.chars
    first, last = chars.shift, chars.pop
    chars.push(first)
    chars.unshift(last)
    chars.join
  end
  puts words.join(' ')
end