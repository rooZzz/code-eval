File.open('input.txt').readlines.each do |line|
  puts line.scan(/[a-zA-Z]+/).map(&:downcase).join(' ')
end