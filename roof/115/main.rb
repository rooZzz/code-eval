ARGV[0] = 'input.txt'

File.open(ARGV[0]).each_line do |line|
  numbers, words = line.strip.split(',').partition { |element| element.match(/[\d]+/) }
  if numbers.length > 0 && words.length > 0
    puts "#{words.join(',')}|#{numbers.join(',')}"
  elsif numbers.length > 0
    puts numbers.join(',')
  elsif words.length > 0
    puts words.join(',')
  end
end