input = File.open('input.txt').readlines
num_lines_to_output = input.shift.to_i
puts input.map(&:strip).sort { |x, y| x.length <=> y.length }.reverse[0...num_lines_to_output]