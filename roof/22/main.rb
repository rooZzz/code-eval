def fib(n)
  return n if n <= 1
  fib(n - 1) + fib(n - 2)
end

File.open('input.txt').readlines.each do |line|
  puts fib(line.to_i)
end
