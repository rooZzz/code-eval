File.open('input.txt').readlines.each do |line|
  list = line.strip.split
  index_from_last = list.last.to_i
  puts list[list.length - 1 - index_from_last] if index_from_last < list.length
end