File.open('input.txt').readlines.each do |line|
  value = line.strip.to_i
  num_coins_required = 0
  [5, 3, 1].each do |coin|
    while value - coin >= 0
      num_coins_required += 1
      value -= coin
    end
  end
  puts num_coins_required
end