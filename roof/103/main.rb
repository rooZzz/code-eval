ARGV[0] = 'input.txt'

File.open(ARGV[0]).each_line do |line|
  nums = line.split(' ').map(&:to_i)
  puts [nums.find_index(nums.select { |num| nums.count(num) == 1 }.min) || -1, -1].max + 1
end