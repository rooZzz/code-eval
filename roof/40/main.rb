File.open('input.txt').readlines.each do |line|
  occurrences = {}
  digits = line.strip.chars.map(&:to_i)
  digits.each do |digit|
    occurrences[digit] = occurrences.fetch(digit, 0) + 1
  end
  is_self_describing = true
  digits.each_with_index do |digit, index|
    is_self_describing &&= occurrences.fetch(index, 0) == digit
  end
  puts is_self_describing ? 1 : 0
end