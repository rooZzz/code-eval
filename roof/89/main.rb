triangle = []
max_to_coord = {}
File.open('input.txt').readlines.each do |line|
  triangle << line.strip.split(' ').map(&:to_i)
end
max_to_coord[[0, 0]] = triangle.shift[0]
triangle.each_with_index do |layer, i|
  layer.each_with_index do |element, j|
    a, b = -1, -1
    a = element + max_to_coord[[i, j-1]] if j > 0
    b = element + max_to_coord[[i, j]] if j < layer.length - 1
    max_to_coord[[i + 1, j]] = [a, b].max
  end
end
puts max_to_coord.select { |key, _| key.first == triangle.length }.values.max