ARGV[0] = 'input.txt'

def gcd(a, b)
  until b.zero?
    t = b
    b = a % b
    a = t
  end
  a
end

def ss_for_customer(customer, product)
  #1. If the number of letters in the product's name is even then the SS is the number of vowels (a, e, i, o, u, y) in the customer's name multiplied by 1.5.
  if product.size.even?
    ss = customer.chars.select { |char| %w(a A e E i I o O u U y Y).include?(char) }.count * 1.5
  #2. If the number of letters in the product's name is odd then the SS is the number of consonants in the customer's name.
  else
    ss = customer.chars.reject { |char| %w(a A e E i I o O u U y Y).include?(char) }.count
  end

  #3. If the number of letters in the product's name shares any common factors (besides 1) with the number of letters in the customer's name then the SS is multiplied by 1.5.
  ss * (gcd(customer.size, product.size) > 1 ? 1.5 : 1)
end

File.open(ARGV[0]).each_line do |line|

  people, products = line.strip.split(';').map { |x| x.split(',') }
  p people
  p products

  people.each do |person|
    puts '*' * 10
    products.each do |product|
      p [person, product, ss_for_customer(person, product)]
    end

  end
  puts '*' * 20


end