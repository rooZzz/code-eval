ARGV[0] = 'input.txt'

File.open(ARGV[0]).each_line do |line|
  input = line.strip.chars
  occurrences = Hash.new(0)
  input.each { |char| occurrences[char.downcase] += 1 if char =~ /[a-zA-Z]/ }
  max_total = 0
  max_current = 26
  until occurrences.empty?
    key, value = occurrences.max_by { |_, v| v }
    max_total += max_current * value
    occurrences.delete(key)
    max_current -= 1
  end
  puts max_total
end