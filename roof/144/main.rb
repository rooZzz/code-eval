ARGV[0] = 'input.txt'

possible_end_nums = {
    2 => [2, 4, 8, 6],
    3 => [3, 9, 7, 1],
    4 => [4, 6],
    5 => [5],
    6 => [6],
    7 => [7, 9, 3, 1],
    8 => [8, 4, 2, 6],
    9 => [9, 1]
}

File.open(ARGV[0]).each_line do |line|
  a, n = line.strip.split.map(&:to_i)
  end_nums = possible_end_nums[a]
  div, mod = n.divmod(end_nums.size)
  occurrences = Hash.new(0)
  end_nums.each { |num| occurrences[num] = div }
  mod.times { |i| occurrences[end_nums[i]] += 1 }
  puts 10.times.map { |i| "#{i}: #{occurrences[i]}" }.join(', ')
end