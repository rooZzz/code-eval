ARGV[0] = 'input.txt'

File.open(ARGV[0]).each_line do |line|
  first, second = line.split(';').map { |nums| nums.split(',').map(&:to_i) }
  puts (first & second).sort.join(',')
end