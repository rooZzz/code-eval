File.open('input.txt').readlines.each do |line|
  rows = line.strip.split(' | ').map { |row| row.split.map(&:to_i) }
  maxes = rows.shift
  rows.each do |row|
    maxes = [row, maxes].transpose.map(&:max)
  end
  puts maxes.join(' ')
end