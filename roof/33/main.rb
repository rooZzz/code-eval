require 'set'
input = []
File.open('input.txt').readlines.each do |line|
  input << line.to_i
end
input.shift
input.each do |num|
  square_set = Set.new
  i = -1
  while (square = (i+=1) ** 2) <= num
      square_set.add(square)
  end
  addends = square_set.select { |x| x <= num / 2 }
  counter = 0
  addends.each do |addend|
    counter += 1 if square_set.include?(num - addend)
  end
  puts counter
end