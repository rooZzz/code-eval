File.open('input.txt').readlines.each do |line|
  nums = line.strip.split(',').map(&:to_i)
  curr_max = nums.first
  (0...nums.size).each do |i|
    (i...nums.size).each do |j|
      curr_max = [curr_max, nums[i..j].inject(:+)].max
    end
  end
  puts curr_max
end