=begin
> - move to the next cell;
< - move to the previous cell;
+ - increment the value in the current cell by 1;
- - decrement the value of the current cell by 1;
. - output the value of the current cell;
, - input the value outside and store it in the current cell;
[ - if the value of the current cell is zero, move forward on the text to the program to] taking into account nesting;
] - if the value of the current cell is not zero, go back on the text of the program to [considering nesting;
=end
ARGV[0] = 'input.txt'

def fast_forward(input, curr_index)
  open_count = 0
  while true
    curr_index += 1
    if input[curr_index] == '['
      open_count += 1
    elsif input[curr_index] == ']'
      if open_count == 0
        break
      else
        open_count -= 1
      end
    end
  end
  curr_index
end

File.open(ARGV[0]).each_line do |line|

  input = line.strip.chars

  index = 0
  cells = [0]
  cell_index = 0
  loop_index_stack = []
  output = []

  while index < input.size
    instruction = input[index]
    case instruction
      when '+'
        cells[cell_index] = (cells[cell_index] + 1) % 256
      when '-'
        cells[cell_index] = (cells[cell_index] - 1) % 256
      when '>'
        cell_index += 1
        cells.push(0) if cells.length == cell_index
      when '<'
        cell_index -= 1
        if cell_index == -1
          cells.unshift(0)
          cell_index = 0
        end
      when ','
        char_input = $stdin.gets
        cells[cell_index] = char_input.strip.ord
      when '.'
        output << cells[cell_index]
      when '['
        if cells[cell_index] == 0
          index = fast_forward(input, index)
        else
          loop_index_stack.push(index)
        end
      when ']'
        if cells[cell_index] != 0
          index = loop_index_stack.last
        else
          loop_index_stack.pop
        end
      else
        raise "Unrecognized command: #{instruction}"
    end
    index += 1
  end

  puts output.map(&:chr).join
end