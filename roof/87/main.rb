ARGV[0] = 'input.txt'

COLS = 256
ROWS = 256
grid = Array.new(ROWS) { Array.new(COLS) { 0 } }

File.open(ARGV[0]).each_line do |line|

  input = line.strip.split(' ')
  command = input.shift
  parameters = input.map(&:to_i)

  case command
    when 'SetRow'
      (0...COLS).each { |index| grid[parameters.first][index] = parameters.last }
    when 'SetCol'
      (0...ROWS).each { |index| grid[index][parameters.first] = parameters.last }
    when 'QueryRow'
      puts grid[parameters.first][0..COLS].inject(:+)
    when 'QueryCol'
      puts grid[0..ROWS].map { |row| row[parameters.first] }.inject(:+)
    else
      fail("Unknown command provided: #{command}")
  end

end