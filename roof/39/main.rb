ARGV[0] = 'input.txt'

LOOP_THRESHOLD = 100

File.open(ARGV[0]).each_line do |line|
  input = line.to_i
  num_loops = 0
  while input != 1 && (num_loops += 1) <= LOOP_THRESHOLD
    input = input.to_s.chars.map { |char| char.to_i ** 2 }.inject(:+)
  end
  puts input == 1 ? 1 : 0
end