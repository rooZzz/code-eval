File.open('input.txt').readlines.each do |line|
  x, y, max_num = line.split(' ').map(&:to_i)
  puts max_num.times.map { |num|
    num += 1
    out = ''
    out += 'F' if num % x == 0
    out += 'B' if num % y == 0
    out.empty? ? num : out
  }.join(' ')
end