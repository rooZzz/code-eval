File.open('input.txt').readlines.each do |line|

  puts line.strip.chars.map { |char|
    case char
      when 'a'..'z'
        char.upcase
      when 'A'..'Z'
        char.downcase
      else
        char
    end
  }.join

end