ARGV[0] = 'input.txt'

File.open(ARGV[0]).each_line do |line|
  input = line.strip
  puts (input.to_i == input.chars.map { |char| char.to_i ** input.length }.inject(:+)).to_s.capitalize
end