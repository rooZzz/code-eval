File.open('input.txt').readlines.each do |line|
  n, m = line.strip.split(',').map(&:to_i)
  n -= m while n >= m
  puts n
end