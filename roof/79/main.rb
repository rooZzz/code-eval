ARGV[0] = 'input.txt'

def gen_matrix(m, n, matrix_row_major)
  matrix = []
  (0...n).each do |i|
    row = []
    (0...m).each do |j|
      row << matrix_row_major[i*m + j]
    end
    matrix << row
  end
  matrix
end

def minesweep(m, n, matrix)
  mineswept = []
  (0...n).each do |i|
    row = []
    (0...m).each do |j|
      if matrix[i][j] == '*'
        row << '*'
      else
        mine_counter = 0
        mine_counter += 1 if i > 0 && matrix[i-1][j] == '*'
        mine_counter += 1 if i > 0 && j < m-1 && matrix[i-1][j+1] == '*'
        mine_counter += 1 if j < m-1 && matrix[i][j+1] == '*'
        mine_counter += 1 if i < n-1 && j < m-1 && matrix[i+1][j+1] == '*'
        mine_counter += 1 if i < n-1 && matrix[i+1][j] == '*'
        mine_counter += 1 if i < n-1 && j > 0 && matrix[i+1][j-1] == '*'
        mine_counter += 1 if j > 0 && matrix[i][j-1] == '*'
        mine_counter += 1 if i > 0 && j > 0 && matrix[i-1][j-1] == '*'
        row << mine_counter
      end
    end
    mineswept << row
  end
  mineswept
end

File.open(ARGV[0]).each_line do |line|
  size, matrix_row_major = line.strip.split(';')
  n, m = size.split(',').map(&:to_i)
  puts minesweep(m, n, gen_matrix(m, n, matrix_row_major)).join
end