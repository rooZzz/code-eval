class Fixnum

  def is_palindrome?
    str = self.to_s
    str == str.reverse
  end

end

File.open('input.txt').readlines.each do |line|
  first_in_range, second_in_range = line.strip.split.map(&:to_i)
  total_interesting = 0
  (first_in_range..second_in_range).each do |num|
    (num..second_in_range).each do |sub_num|
      total_interesting += 1 if (num..sub_num).to_a.select(&:is_palindrome?).count.even?
    end
  end
  puts total_interesting
end
