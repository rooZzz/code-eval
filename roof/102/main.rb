ARGV[0] = 'input.txt'

require 'json'

File.open(ARGV[0]).each_line do |line|
  input = line.strip
  next if input.empty?
  json = JSON.parse(input)
  output = json['menu']['items'].select { |item| item && item.include?('label') }.map { |item| item['id'] }.inject(:+)
  puts output || 0
end