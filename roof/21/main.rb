ARGV[0] = 'input.txt'

File.open(ARGV[0]).each_line do |line|
  puts line.strip.chars.map(&:to_i).inject(:+)
end