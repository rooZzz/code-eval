require 'set'

# noinspection ALL
class Fixnum

  @@primes = Set.new

  def is_prime?
    return true if @@primes.include?(self)
    return false if self <= 1
    (2..Math.sqrt(self).floor).each do |divisor|
      return false if self % divisor == 0
    end
    @@primes.add(self)
    true
  end

end

counter = 1
primes_array = []
while primes_array.count < 1000
  counter += 1
  primes_array << counter if counter.is_prime?
end
puts primes_array.inject(:+)