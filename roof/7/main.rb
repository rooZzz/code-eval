ARGV[0] = 'input.txt'


File.open(ARGV[0]).each_line do |line|
  possible_operators = '*/+'
  expression = line.strip.split(' ')
  until expression.size == 1
    index = 0
    while index <= expression.size - 2
      if possible_operators.include?(expression[index]) && !possible_operators.include?(expression[index+1]) && !possible_operators.include?(expression[index+2])
        pre = index == 0 ? [] : expression[0..index-1]
        evaluation = eval("#{expression[index+1].to_i}#{expression[index]}#{expression[index+2].to_i}").to_s
        post = expression[index+3..-1]
        expression = pre + [evaluation] + post
      end
      index += 1
    end
  end

  puts expression.first
end
