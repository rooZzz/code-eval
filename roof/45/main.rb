class Fixnum

  def palindrome
    self.to_s.reverse.to_i
  end

  def is_palindrome?
    self == self.palindrome
  end

end

File.open('input.txt').readlines.each do |line|
  input = line.to_i
  counter = 0
  until input.is_palindrome?
    input += input.palindrome
    counter += 1
  end
  puts "#{counter} #{input}"
end