ARGV[0] = 'input.txt'

def gen_matrix(n, m, matrix_row_major)
  matrix = []
  (0...n).each do |i|
    row = []
    (0...m).each do |j|
      row << matrix_row_major[i*m + j]
    end
    matrix << row
  end
  matrix
end

File.open(ARGV[0]).each_line do |line|
  n, m, elements = line.strip.split(';')
  n = n.to_i
  m = m.to_i
  elements = elements.split
  matrix = gen_matrix(n, m, elements)
  output = []
  counter = n * m

  (0..n/2).each do |inset|

    (inset...m-inset).each do |i|
      output << matrix[inset][i] if counter > 0
      counter -= 1
    end

    (inset+1...n-inset).each do |i|
      output << matrix[i][m-1-inset] if counter > 0
      counter -= 1
    end

    (inset...m-inset-1).reverse_each do |i|
      output << matrix[n-inset-1][i] if counter > 0
      counter -= 1
    end

    (inset+1...n-inset-1).reverse_each do |i|
      output << matrix[i][inset] if counter > 0
      counter -= 1
    end

  end

  puts output.join(' ')

end