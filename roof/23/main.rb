output = []
(1..12).each do |i|
  line = ''
  (1..12).each do |j|
    line += (i * j).to_s.rjust(4, ' ')
  end
  output << line.strip
end
puts output