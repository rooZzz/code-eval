ARGV[0] = 'input.txt'

def blocks_contain_word(blocks, word)
  return true if word.empty?
  ret_val = false
  char = word.first
  blocks.each_with_index do |block, index|
    if block.include?(char)
      remaining_blocks = (index == 0 ? [] : blocks[0..index-1]) + blocks[index+1..-1]
      ret_val ||= blocks_contain_word(remaining_blocks, word[1..-1])
    end
  end
  ret_val
end

File.open(ARGV[0]).each_line do |line|
  _, word, blocks = line.strip.split(' | ')
  blocks = blocks.split.map { |block| block.chars }
  puts blocks_contain_word(blocks, word.chars).to_s.capitalize
end
