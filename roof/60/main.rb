require 'set'

def traverse(x, y, traversed_points=Set.new)
  if (x.to_s + y.to_s).scan(/\d/).map(&:to_i).inject(:+) <= 19
    traversed_points.add([x, y])
    traverse(x + 1, y, traversed_points) unless traversed_points.include?([x + 1, y])
    traverse(x - 1, y, traversed_points) unless traversed_points.include?([x - 1, y])
    traverse(x, y + 1, traversed_points) unless traversed_points.include?([x, y + 1])
    traverse(x, y - 1, traversed_points) unless traversed_points.include?([x, y - 1])
  end
  traversed_points
end

puts traverse(0, 0).count