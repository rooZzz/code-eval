File.open('input.txt').readlines.each do |line|
  sentence, blacklist = line.split(',').map(&:strip)
  puts sentence.chars.reject { |char| blacklist.include?(char) }.join
end