class StringPermutations

  class << self

    def get_ordered_permutations(input_file)

      File.open(input_file).readlines.each do |line|
        puts get_permutations(line.strip.chars).sort.map(&:join).join(',')
      end

    end

    private

    def get_permutations(input)
      permutations = []
      input.each_with_index do |char, index|
        remaining_chars = input.dup
        remaining_chars.delete_at(index)
        get_permutations(remaining_chars).each do |permutation|
          permutations << [char] + permutation
        end
        permutations = input.map { |x| [x] } if permutations.empty?
      end
      permutations
    end

  end

end

StringPermutations.get_ordered_permutations('input.txt')