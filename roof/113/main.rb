ARGV[0] = 'input.txt'

File.open(ARGV[0]).each_line do |line|
  a, b = line.split(' | ').map { |list| list.split.map(&:to_i) }
  puts a.zip(b).map { |i, j| i*j }.join(' ')
end