class Fixnum

  def is_ugly?
    [2, 3, 5, 7].map { |single_digit_prime| self % single_digit_prime == 0 }.any?
  end

end

class UglyNumberCalculator

  class << self

    def get_ugly_num_counts(file)
      File.open(file).readlines.map do |line|
        total_ugly = 0
        number = line.strip.split('')
        get_num_permutations(number).each do |permutation|
          total_ugly += get_possible_totals(permutation).select { |total| total.is_ugly? }.count
        end
        total_ugly
      end
    end

    private

    def get_num_permutations(nums)
      permutations = []
      nums.length.times do |i|
        first_num = nums[0..i].join.to_i
        remaining_nums = nums[i+1..nums.length]
        if remaining_nums.empty?
          permutations << [first_num]
        else
          get_num_permutations(remaining_nums).each do |last|
            permutations << [first_num] + last
          end
        end
      end
      permutations
    end

    def get_possible_totals(permutation)
      running_totals = [permutation.shift]
      permutation.each do |num|
        new_running_totals = []
        running_totals.each do |total|
          new_running_totals << total + num
          new_running_totals << total - num
        end
        running_totals = new_running_totals
      end
      running_totals
    end

  end

end

puts UglyNumberCalculator.get_ugly_num_counts('input.txt')