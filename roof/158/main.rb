ARGV[0] = 'input.txt'

def bubble_sort(array, iterations)
  iterations_passed = -1
  swapped = true
  while swapped && iterations != (iterations_passed += 1)
    swapped = false
    (1...array.length).each do |i|
      (array[i-1], array[i] = array[i], array[i-1]; swapped = true) if array[i-1] > array[i]
    end
  end
  array
end

File.open(ARGV[0]).each_line do |line|
  array, iterations = line.strip.split('|')
  array = array.split(' ').map(&:to_i)
  iterations = iterations.to_i
  puts bubble_sort(array, iterations).join(' ')
end