File.open('input.txt').readlines.each do |line|
  puts line.to_i.to_s(2).chars.select { |x| x == '1' }.count
end