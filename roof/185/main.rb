File.open('input.txt').readlines.each do |line|
  pieces = line.split('|').reject { |piece| piece.empty? }
  ordered = pieces.shift
  until pieces.empty?
    candidates_to_prepend = []
    candidates_to_append = []
    pieces.each_with_index do |piece, index|
      if piece[1..-1] == ordered[0..piece.size-2] && candidates_to_append.empty?
        candidates_to_prepend << [piece, index]
      elsif ordered[-piece.size+1..-1] == piece[0..piece.size-2] && candidates_to_prepend.empty?
        candidates_to_append << [piece, index]
      end
    end

    if candidates_to_prepend.count > 1
      candidates_to_prepend.reject! { |candidate| candidate.first.include?(' ') }
    end

    if candidates_to_append.count > 1
      candidates_to_append.reject! { |candidate| candidate.include?(' ') }
    end

    if candidates_to_append.first
      piece, index = candidates_to_append.first
      ordered = ordered + piece[-1]
      pieces.delete_at(index)
    end

    if candidates_to_prepend.first
      piece, index = candidates_to_prepend.first
      ordered = piece[0] + ordered
      pieces.delete_at(index)
    end

  end
  puts ordered
end