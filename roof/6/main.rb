def find_largest_contiguous(first_word, second_word)
  contiguous_matches = []
  (0...first_word.size).each do |i|
    matches = []
    index_in_second_word = 0
    (i...first_word.size).each do |j|
      first_word_letter = first_word[j]
      (index_in_second_word...second_word.size).each do |k|
        if first_word_letter == second_word[k]
          index_in_second_word = k + 1
          matches << first_word_letter
          break
        end
      end
    end
    contiguous_matches << matches
  end
  contiguous_matches.max_by(&:size).join
end

File.open('input.txt').readlines.each do |line|
  input = line.strip
  next if input.empty?
  first_word, second_word = input.split(';')
  largest_contiguous = [find_largest_contiguous(first_word, second_word), find_largest_contiguous(second_word, first_word)].max_by(&:size)
  puts largest_contiguous unless largest_contiguous.empty?
end