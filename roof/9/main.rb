File.open('input.txt').readlines.each do |line|
  nums = line.strip.split(' ').reverse
  i = -1
  nums.select! do |_|
    i += 1
    i.even?
  end
  puts nums.join(' ')
end